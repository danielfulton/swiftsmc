import Foundation

struct SwiftSMC {
    struct Transition {
        var initialState: String = ""
        var event: String = ""
        var nextState: String = ""
        var action: String = ""
        
        mutating func add(_ text: String) {
            if initialState.isEmpty {
                initialState = text
                return
            }
            if event.isEmpty {
                event = text
                return
            }
            if nextState.isEmpty {
                nextState = text
                return
            }
            if action.isEmpty {
                action = text
                return
            }
        }
        func isFUll() -> Bool {
            return !action.isEmpty
        }
    }
    func compile(_ text: String) {
        let scanner = Scanner(string: text)
        guard let initialState = scanValueForToken(scanner, token: "Initial") else {
            return
        }
        guard let fsm = scanValueForToken(scanner, token: "FSM") else {
            return
        }
        if scanner.scanString("{") == nil {
            print("missing open brace.")
            return
        }
        var transitions: [Transition] = []
        var currentTransition = Transition()
        while let nextToken = scanner.scanCharacters(from: .alphanumerics) {
            currentTransition.add(nextToken)
            if currentTransition.isFUll() {
                transitions.append(currentTransition)
                currentTransition = Transition()
            }
        }
        if scanner.scanString("}") == nil {
            print("missing closing brace.")
            return
        }
        let initials = transitions.map({ $0.initialState })
        let nexts = transitions.map({ $0.nextState })
        let states: Set<String> = Set(initials + nexts)
        
        var stateEnumText = "enum \(fsm)State {\n"
        states.forEach { (state) in
            stateEnumText.append("case \(state)\n")
        }
        stateEnumText.append("""
            init() {
            self = .\(initialState)
            }
            
            """)
        stateEnumText.append("}\n")
        
        let events = Set(transitions.map({ $0.event }))
        stateEnumText.append("enum \(fsm)Event {\n")
        events.forEach { (event) in
            stateEnumText.append("case \(event)\n")
        }
        stateEnumText.append("}\n")
        
        let actions = Set(transitions.map({ $0.action }))
        stateEnumText.append("protocol \(fsm)Actions {\n")
        actions.forEach { (action) in
            stateEnumText.append("func \(action)()\n")
        }
        stateEnumText.append("}\n")
        
        stateEnumText.append("""
            protocol \(fsm): \(fsm)Actions {
            init()
            var state: \(fsm)State { get set }
            mutating func handleEvent(_ event: \(fsm)Event)
            }
            
            """)
        
        stateEnumText.append("extension \(fsm) {\n")
        events.forEach { (event) in
            stateEnumText.append("""
                mutating func \(event)() {
                handleEvent(.\(event))
                }
                
                """)
        }
        
        stateEnumText.append("""
            mutating func handleEvent(_ event: \(fsm)Event) {
            switch state {
            
            """)
        
        states.forEach { (state) in
            stateEnumText.append("""
                case .\(state):
                switch event {
                
                """)
            transitions.filter({ $0.initialState == state }).forEach { (transition) in
                stateEnumText.append("""
                    case .\(transition.event):
                    state = .\(transition.nextState)
                    \(transition.action)()
                    
                    """)
            }
            stateEnumText.append("}\n") // event switch
        }
        
        stateEnumText.append("}\n") // state switch
        stateEnumText.append("}\n") // function
        stateEnumText.append("}\n") // extension

        let fileManager = FileManager()
        guard let stringData = stateEnumText.data(using: .utf8) else {
            print("Error creating data from string.")
            return
        }
        fileManager.createFile(atPath: "./\(fsm).swift", contents: stringData, attributes: nil)
    }
    
    func scanValueForToken(_ scanner: Scanner, token: String) -> String? {
        guard scanner.scanString(token) != nil else {
            print("error: no initial state")
            return nil
        }
        guard scanner.scanString(":") != nil else {
            print("error: no initial state")
            return nil
        }
        guard let initialState = scanner.scanCharacters(from: .alphanumerics) else {
            print("error: no initial state")
            return nil
        }
        return initialState
    }
    
    func scanFSMName(_ scanner: Scanner) -> String? {
        guard scanner.scanString("FSM") != nil else {
            print("error: no FSM name")
            return nil
        }
        guard scanner.scanString(":") != nil else {
            print("error: no FSM name")
            return nil
        }
        guard let fsm = scanner.scanCharacters(from: .alphanumerics) else {
            print("error: no FSM name")
            return nil
        }
        return fsm
    }
}
