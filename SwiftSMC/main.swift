import Foundation
run()
func run() {
    guard CommandLine.arguments.count == 2 else {
        print("Please specify input file with .fsm extension.")
        return
    }
    let fileNameArg = CommandLine.arguments[1]
    let url = URL(fileURLWithPath: fileNameArg)
    do {
        let text = try String(contentsOf: url)
        print("made string from input")
        if text.isEmpty {
            print("empty argument")
            return
        }
        print("successful compile")
        SwiftSMC().compile(text)
    } catch {
        print("Error getting contents of URL: \(url.absoluteString)")
        print(error.localizedDescription)
    }
}


