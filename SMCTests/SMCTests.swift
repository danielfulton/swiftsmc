import XCTest

class SMCTests: XCTestCase {

    func testSomething() {
        var compiler = SwiftSMC()
        let bundle = Bundle(for: SMCTests.self)
        let testFileURL = bundle.url(forResource: "Turnstile", withExtension: "fsm")!
        let text = try! String(contentsOf: testFileURL, encoding: .utf8)
        compiler.compile(text)
        let resultURL = URL(fileURLWithPath: "./Turnstile.swift")
        XCTAssertNotNil(resultURL)
        
    }

}
